const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const Schema = mongoose.Schema;

const ProgressSchema = new Schema({
    title: String,
    startWeight: Number,
    endWeight: Number,
    height: Number,
    gender: {
        type: String,
        enum: ['MALE', 'FEMALE'],
    },
    age: Number,
    url: String,
    permalink: String,
    postId: { 
        type: String,
        unique: true,
     },
    over_18: Boolean,
    date: Date,
});

ProgressSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Progress', ProgressSchema);