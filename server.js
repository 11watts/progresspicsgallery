const config = require('config');
const cors = require('cors')
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const titleToData = require('./utils/titleToData');
const Progress = require('./models/Progress');

const app = express();
const PORT = config.get('expressPort');
const POST_PER_PAGE = config.get('postsPerPage');

const dbSetup = () => {
    mongoose.connect(config.get('mongoURL'), { useNewUrlParser: true });
    mongoose.Promise = global.Promise;
};

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/', async (req, res) => {
    try {
        const {
            minStartWeight,
            maxStartWeight,
            minEndWeight,
            maxEndWeight,
            minFt,
            minIn,
            maxFt,
            maxIn,
            minAge,
            maxAge,
            nsfw,
            page,
        } = req.query;
        const minHeight = parseInt(minFt) * 12 + parseInt(minIn);
        const maxHeight = parseInt(maxFt) * 12 + parseInt(maxIn);
        console.log(req.query);
        let count = await Progress.count({
            startWeight: { $gt: minStartWeight, $lt: maxStartWeight },
            endWeight: { $gt: minEndWeight, $lt: maxEndWeight },
            height: { $gt: minHeight, $lt: maxHeight },
            age: { $gt: minAge, $lt: maxAge },
            over_18: [false, nsfw],
        });
        console.log(`Found ${count} count`);

        let docs = await Progress.find({
            startWeight: { $gt: minStartWeight, $lt: maxStartWeight },
            endWeight: { $gt: minEndWeight, $lt: maxEndWeight },
            height: { $gt: minHeight, $lt: maxHeight },
            age: { $gt: minAge, $lt: maxAge },
            over_18: [false, nsfw],
        }).skip(POST_PER_PAGE * page).limit(POST_PER_PAGE);
        console.log(`Found ${docs.length} docs`);

        setTimeout(() => {
            res.json({docs, count});
        }, 1000)
    } catch (err) {
        console.error(err)
        res.json({ error: true });
    }

});

app.listen(PORT, () => console.log(`Progress Pics app server listening on port ${PORT}.`));

dbSetup();