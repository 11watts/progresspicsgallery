const expect = require('chai').expect;
const titleToData = require('../utils/titleToData');

const mockTitles = [
    "F/31/ 5’10”[286>261=25lbs] test title 1",
    "F/28/5’2” [254.4lbs>204lbs=50lbsdown] test title 2",
    "F/23 /5’1” [137lbs>115=22lbs] test title 3",
    "M/ 28 /6'1\" [265 > 190 = 75 lbs] (7 months) test title 4",
    "F/021/5'4\" [112 lbs > 106 lbs = -6 lbs] test title 5",
];

const mockTitlesErrors = [
    "24/M/6'3 [250 -->200 pounds] 7 month progress.",
];

const fakePersonInfo = [];
const fakePersonWeight = [];

const height = [
    70,
    62,
    61,
    73,
    64,
];

const age = [
    31,
    28,
    23,
    28,
    21,
];

const gender = [
    'FEMALE',
    'FEMALE',
    'FEMALE',
    'MALE',
    'FEMALE',
];

const weight = [
    { startWeight: 286, endWeight: 261 },
    { startWeight: 254.4, endWeight: 204 },
    { startWeight: 137, endWeight: 115 },
    { startWeight: 265, endWeight: 190 },
    { startWeight: 112, endWeight: 106 },
    { startWeight: 250, endWeight: 200 },
];

describe('getPersonInfo()', function () {
    mockTitles.forEach(k => {
        const personInfo = titleToData.getPersonInfo(k);
        fakePersonInfo.push(personInfo);
    });

    it('should get corrent height', function () {
        fakePersonInfo.forEach((k, i) => {
            expect(k.height).to.be.equal(height[i]);
        });
    });

    it('should get corrent age', function () {
        fakePersonInfo.forEach((k, i) => {
            expect(k.age).to.be.equal(age[i]);
        });
    });

    it('should get corrent gender', function () {
        fakePersonInfo.forEach((k, i) => {
            expect(k.gender).to.be.equal(gender[i]);
        });
    });

    "24/M/6'3 [250 -->200 pounds] 7 month progress."

    it('should throw gender error', function () {
        const t = "24/M/6'3 [250 >210 pounds] 11 month progress.";
        expect(() => titleToData.getPersonInfo(t)).to.throw(`Cannot read gender in ${t}`);
    });

});

describe('getWeight()', function () {
    mockTitles.forEach(k => {
        const personWeight = titleToData.getWeight(k);
        fakePersonWeight.push(personWeight);
    });

    it('should get corrent weight', function () {
        fakePersonWeight.forEach((k, i) => {
            expect(k).to.deep.equal(weight[i]);
        });
    });

    it('should throw startWeight error', function () {
        const t = "M/24/6'3 [one hundred and fifth > 110 pounds] 11 month progress.";
        expect(() => titleToData.getWeight(t)).to.throw(`Cannot read startWeight in ${t}`);
    });
});