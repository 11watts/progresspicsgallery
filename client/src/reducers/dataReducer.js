import {
    RECEIVE_DATA,
    LOADING_DATA,
    LOADING_ERROR,
    HIDE_POST,
    CHANGE_PAGE,
} from '../actions/data'


const initialState = {
    data: [],
    error: false,
    isLoading: false,
    pageNumber: 0,
    totalPosts: 0,
};

export default (state = initialState, action) => {
    switch (action.type) {
        case RECEIVE_DATA:
            return { ...state, data: action.data, totalPosts: action.count, isLoading: false };
        case LOADING_DATA:
            return { ...state, isLoading: action.isLoading };
        case LOADING_ERROR:
            return { ...state, error: true, isLoading: false };
        case HIDE_POST:
            const data = state.data.filter(i => i._id !== action._id);
            return { ...state, data };
        case CHANGE_PAGE:
            return { ...state, pageNumber: action.page };
        default:
            return state;
    }
}
