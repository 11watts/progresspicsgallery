import {
    UPDATE_FILTER,
} from '../actions/filter'


const initialState = {
    minStartWeight: 0,
    maxStartWeight: 999,
    minEndWeight: 0,
    maxEndWeight: 999,
    minFt: 0,
    minIn: 0,
    maxFt: 7,
    maxIn: 11,
    minAge: 0,
    maxAge: 100,
    nsfw: false,
};

export default (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_FILTER:
            return action.filter;
        default:
            return state;
    }
}
