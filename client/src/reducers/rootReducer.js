import { combineReducers } from 'redux';
import dataReducer from './dataReducer';
import filterReducer from './filterReducer';

const rootReducer = combineReducers({
    dataReducer,
    filterReducer,
});

export default rootReducer;