import axios from 'axios';
import { updateFilter } from './filter';

export const RECEIVE_DATA = 'RECEIVE_DATA';
export const LOADING_DATA = 'LOADING_DATA';
export const LOADING_ERROR = 'LOADING_ERROR';
export const HIDE_POST = 'HIDE_POST';
export const CHANGE_PAGE = 'CHANGE_PAGE';

const URL = 'http://127.0.0.1:3001';

export function receiveData(data, count) {
    return { type: RECEIVE_DATA, data, count };
}

export function loadingData(bool) {
    return {
        type: LOADING_DATA,
        isLoading: bool,
    };
}

export function loadingError() {
    return {
        type: LOADING_ERROR,
    };
}

export function fetchData({
    minStartWeight = 0,
    maxStartWeight = 999,
    minEndWeight = 0,
    maxEndWeight = 999,
    minFt = 0,
    minIn = 0,
    maxFt = 7,
    maxIn = 11,
    minAge = 0,
    maxAge = 100,
    nsfw = false,
    page = 0,
} = {}) {
    return dispatch => {
        dispatch(loadingData(true));
        dispatch(updateFilter({
            minStartWeight,
            maxStartWeight,
            minEndWeight,
            maxEndWeight,
            minFt,
            minIn,
            maxFt,
            maxIn,
            minAge,
            maxAge,
            nsfw,
        }));
        axios.get(URL, {
            params: {
                minStartWeight,
                maxStartWeight,
                minEndWeight,
                maxEndWeight,
                minFt,
                minIn,
                maxFt,
                maxIn,
                minAge,
                maxAge,
                nsfw,
                page,
            }
        }).then(res => {
            if (!Array.isArray(res.data.docs))
                throw new Error('Server did not return data');
            dispatch(receiveData(res.data.docs, res.data.count));
        }).catch(err => {
            console.error(err);
            dispatch(loadingError());
        })
    };
}

export function hidePost(_id) {
    return {
        type: HIDE_POST,
        _id,
    };
}

export function changePage(page) {
    return {
        type: CHANGE_PAGE,
        page,
    };
}