import React from 'react'
import { connect } from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';
import GalleryItem from './GalleryItem';
import { Grid, Row, Col } from 'react-flexbox-grid';

const mapStateToProps = ({ dataReducer }) => {
  return dataReducer;
};


export default connect(mapStateToProps)(function Gallery(props) {
  if (props.isLoading) {
    return (
      <Row>
        <Col xs={12}>
          <Row center="xs">
            <Col xs={1}>
              <CircularProgress centered="true"/>
            </Col>
          </Row>
        </Col>
      </Row>
    );
  }

  return (
    <Grid>
      <Row>
        {
          props.data.map((d, i) => {
            return (
              <Col key={i} xs={12} sm={6} md={4} lg={3}>
                <GalleryItem key={i} data={d} />
              </Col>
            );
          })
        }
      </Row>
      <Row center="xs">
        <Col xs={8}>
          {props.children}
        </Col>
      </Row>
    </Grid>
  )
});