import React, { Component } from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import { connect } from 'react-redux';
import { fetchData } from '../actions/data';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  root: {
    display: 'flex',
  },
  margin: {
    margin: theme.spacing.unit,
  },
  hide: {
    display: 'none',
  },
  heightInput: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: '100px',
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: '0 8px',
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  },
});

const ftRange = [...Array(7).keys()].map(i => i + 1).map(i => {
  return { value: i, label: `${i} ft` };
});

const inRange = [...Array(12).keys()].map(i => {
  return { value: i, label: `${i} in` };
});

class FilterMenu extends Component {
  state = {
    open: false,
    minFt: 1,
    minIn: 0,
    maxFt: 7,
    maxIn: 11,
    minAge: 0,
    maxAge: 100,
    minStartWeight: 0,
    maxStartWeight: 999,
    minEndWeight: 0,
    maxEndWeight: 999,
    btnDisabled: true,
    nsfw: false,
  };

  handleChange = prop => event => {
    this.setState({ [prop]: event.target.value, btnDisabled: false });
  };

  filter = () => {
    this.props.dispatch(fetchData(this.state));
    this.setState({ btnDisabled: true });
  };

  render() {
    const { classes, theme, handleDrawerClose } = this.props;
    const {
      btnDisabled,
      minStartWeight,
      minEndWeight,
      maxStartWeight,
      maxEndWeight,
      minFt,
      minIn,
      maxFt,
      maxIn,
      minAge,
      maxAge,
      nsfw,
    } = this.state;

    return (
      <>
        <div className={classes.drawerHeader}>
          <Typography variant="h5" color="inherit">
            Filter Results
          </Typography>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
          </IconButton>
        </div>
        <Divider />
        <TextField
          className={classNames(classes.margin)}
          label="Min Start Weight"
          value={minStartWeight}
          onChange={this.handleChange('minStartWeight')}
          type="number"
          InputProps={{
            endAdornment: <InputAdornment>lbs</InputAdornment>,
          }}
        />
        <TextField
          className={classNames(classes.margin)}
          label="Max Start Weight"
          value={maxStartWeight}
          onChange={this.handleChange('maxStartWeight')}
          type="number"
          InputProps={{
            endAdornment: <InputAdornment>lbs</InputAdornment>,
          }}
        />
        <Divider />
        <TextField
          className={classNames(classes.margin)}
          label="Min End Weight"
          value={minEndWeight}
          onChange={this.handleChange('minEndWeight')}
          type="number"
          InputProps={{
            endAdornment: <InputAdornment>lbs</InputAdornment>,
          }}
        />
        <TextField
          className={classNames(classes.margin)}
          label="Max End Weight"
          value={maxEndWeight}
          onChange={this.handleChange('maxEndWeight')}
          type="number"
          InputProps={{
            endAdornment: <InputAdornment>lbs</InputAdornment>,
          }}
        />
        <Divider />
        <div className={classes.container}>
          <TextField
            className={classNames(classes.margin, classes.heightInput)}
            label="Min Age"
            value={minAge}
            onChange={this.handleChange('minAge')}
            type="number"
            InputProps={{
              endAdornment: <InputAdornment>years</InputAdornment>,
            }}
          />
          <TextField
            className={classNames(classes.margin, classes.heightInput)}
            label="Max Age"
            value={maxAge}
            onChange={this.handleChange('maxAge')}
            type="number"
            InputProps={{
              endAdornment: <InputAdornment>years</InputAdornment>,
            }}
          />
        </div>
        <Divider />
        <div className={classes.container}>
          <TextField
            select
            className={classNames(classes.margin, classes.heightInput)}
            label="Min Feet"
            value={minFt}
            onChange={this.handleChange('minFt')}
          >
            {ftRange.map(option => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>
          <TextField
            select
            className={classNames(classes.margin, classes.heightInput)}
            label="Min Inches"
            value={minIn}
            onChange={this.handleChange('minIn')}
          >
            {inRange.map(option => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>
        </div>
        <Divider />
        <div className={classes.container}>
          <TextField
            select
            className={classNames(classes.margin, classes.heightInput)}
            label="Max Feet"
            value={maxFt}
            onChange={this.handleChange('maxFt')}
          >
            {ftRange.map(option => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>
          <TextField
            select
            className={classNames(classes.margin, classes.heightInput)}
            label="Max Inches"
            value={maxIn}
            onChange={this.handleChange('maxIn')}
          >
            {inRange.map(option => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>
        </div>
        <div className={classes.container}>
          <FormControlLabel
          className={classNames(classes.margin)}
            control={
              <Checkbox
                checked={nsfw}
                onChange={() => this.setState({nsfw: !nsfw, btnDisabled: false})}
              />
            }
            label="Include NSFW"
          />
        </div>
        <Divider />
        <Button
          variant="contained"
          disabled={btnDisabled}
          className={classes.button}
          onClick={this.filter}
        >
          Filter
        </Button>
      </>
    );
  }
}
export default withStyles(styles, { withTheme: true })(connect()(FilterMenu));
