import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { Card, CardMedia, CardContent, Typography } from '@material-ui/core';
import { hidePost } from '../actions/data'
const styles = {
    card: {
        minWidth: 240,
        maxWidth: 480,
        margin: '8px',
    },
    media: {
        hight: 0,
    },
};

export default withStyles(styles)(connect()(function GalleryItem(props) {
    const { classes, dispatch } = props;
    const { url, title, _id, permalink } = props.data;

    return (
        <Card className={classes.card}>
            <CardMedia
                component="img"
                className={classes.media}
                image={url}
            />
            <img style={{ display: 'none' }} src={url} onError={() => {
                dispatch(hidePost(_id));
            }} alt="alt" />
            <CardContent>
                <Typography component="p">
                    <a target="_blank" rel="noopener noreferrer" href={`https://www.reddit.com${permalink}`}>{title}</a>
                </Typography>
            </CardContent>
        </Card>
    )
}));
