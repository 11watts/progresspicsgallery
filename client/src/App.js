import React, { Component } from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

import Pagination from 'material-ui-flat-pagination';
import Gallery from './components/Gallery';
import { connect } from 'react-redux';
import { fetchData, changePage } from './actions/data';
import FilterMenu from './components/FilterMenu';

const drawerWidth = 250;
const postsPerPage = 50;

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  root: {
    display: 'flex',
  },
  appBar: {
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 20,
  },
  margin: {
    margin: theme.spacing.unit,
  },
  hide: {
    display: 'none',
  },
  heightInput: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: '100px',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: '0 8px',
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
});

class App extends Component {
  state = {
    open: false,
  };

  handleClick(offset) {
    const filter = this.props.filter;
    const page = offset;
    this.props.dispatch(changePage(offset));
    this.props.dispatch(fetchData({...filter, page}));
  };

  handleDrawerOpen = () => {
    this.setState({ open: true });
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
  };

  handleChange = prop => event => {
    this.setState({ [prop]: event.target.value });
  };
  componentDidMount() {
    this.props.dispatch(fetchData());
  }

  render() {
    const { classes, totalPosts, page } = this.props;
    const { open } = this.state;

    return (
      <div className={classes.root}>
        <CssBaseline />
        <AppBar
          position="fixed"
          className={classNames(classes.appBar, {
            [classes.appBarShift]: open,
          })}
        >
          <Toolbar disableGutters={!open}>
            <IconButton
              color="inherit"
              aria-label="Open drawer"
              onClick={this.handleDrawerOpen}
              className={classNames(classes.menuButton, open && classes.hide)}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" color="inherit" noWrap>
              Progress Pics Gallery
            </Typography>
          </Toolbar>
        </AppBar>
        <Drawer
          className={classes.drawer}
          variant="persistent"
          anchor="left"
          open={open}
          classes={{
            paper: classes.drawerPaper,
          }}
        >
          <FilterMenu handleDrawerClose={this.handleDrawerClose} />
        </Drawer>
        <main
          className={classNames(classes.content, {
            [classes.contentShift]: open,
          })}
        >
          <div className={classes.drawerHeader} />

          <Gallery>
            <Pagination
              limit={1}
              offset={page}
              total={Math.ceil(totalPosts / postsPerPage)}
              onClick={(e, offset) => this.handleClick(offset)}
            />
          </Gallery>
        </main>
      </div>
    );
  }
}

const mapStateToProps = ({ dataReducer, filterReducer }) => {
  return {
    totalPosts: dataReducer.totalPosts,
    page: dataReducer.pageNumber,
    filter: filterReducer,
  };
};

export default withStyles(styles, { withTheme: true })(connect(mapStateToProps)(App));
