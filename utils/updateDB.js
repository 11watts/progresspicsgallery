const axios = require('axios');
const config = require('config');
const mongoose = require('mongoose');
const Progress = require('../models/Progress');
const titleToData = require('./titleToData');
const storage = require('node-persist');

// Start of Gender/Age/Height [Weight Before > Weight After = Total Amount Lost] format
const debug = false || process.argv[2];
const startUnixTime = 1370374400;
const endDate = process.argv[3] || Math.floor(new Date() / 1000); /*1370374400 + (60 * 60 * 24 * 462);*/
const dayInSeconds = 60 * 60 * 24;


let imgCount = 0;
let goodImgCount = 0;

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

const formatImgUrl = (url) => {
    if (url.endsWith('.jpg'))
        return url;
    else if (url.startsWith('http://imgur.com/')) {
        const i = url.split('http://imgur.com/')[1];
        return `http://i.imgur.com/${i}.jpg`
    }
    else
        throw new Error('Could not find image');
};

const dbSetup = async () => {
    try {
        mongoose.connect(config.get('mongoURL'), { useNewUrlParser: true });
        mongoose.Promise = global.Promise;
    } catch (e) {
        console.error(e)
    }
};

async function loadDay(time) {
    const endTime = time + dayInSeconds;
    const response = await axios.get(`https://api.pushshift.io/reddit/search/submission/?subreddit=progresspics&sort=desc&sort_type=created_utc&after=${time}&before=${endTime}&size=1000`);
    console.log(`Found ${response.data.data.length} progress entries`);
    if (debug)
        await sleep(1000);
    for (data of response.data.data) {
        try {
            imgCount++;
            if (debug)
                await sleep(500);
            console.log(imgCount)
            const { title, url, permalink, id, over_18 } = data;
            const info = titleToData.getPersonInfo(title);

            const weight = titleToData.getWeight(title);
            const newUrl = formatImgUrl(url);
            if (debug)
                await sleep(200);
            console.log('Saving to db...');
            if (debug)
                await sleep(500);
            const progress = await new Progress({
                over_18,
                title,
                url: newUrl,
                permalink,
                postId: id,
                ...info,
                ...weight,
            }).save();
            goodImgCount++;
            console.log(`${progress._id} saved, saved number: ${goodImgCount}`)
            if (debug)
                await sleep(1000);
        } catch (e) {
            console.error('error')
            console.error(e)
        }

    }
    return endTime;
};


(async function () {
    await storage.init();
    await dbSetup();
    let currentTime = parseInt(await storage.getItem('prevEndTime')) || startUnixTime;
    console.log(`Starting at ${currentTime}, ending at ${endDate}`);
    await sleep(5000);
    while (currentTime < endDate) {
        currentTime = await loadDay(currentTime);
        await storage.setItem('prevEndTime', currentTime);
    }
    console.log(`Saved ${goodImgCount}`)
    process.exit();
}());