const axios = require('axios');
const co = require('co');
const config = require('config');
const mongoose = require('mongoose');
const Progress = require('../models/Progress');

let count = 0;
let removeCount = 0;

const dbSetup = async () => {
    try {
        mongoose.connect(config.get('mongoURL'), { useNewUrlParser: true });
        mongoose.Promise = global.Promise;
    } catch (e) {
        console.error(e)
    }
};

const testUrl = async (url) => {
    try {
        const response = await axios.head(url);
        console.log(response.headers['content-type']);
        if(response.headers['content-type'].startsWith('image'))
            return true;
        else 
            return false;
    } catch (e) {
        console.error('error');
        return false;
    }
};

(async function () {
    await dbSetup();
    co(function* () {
        const cursor = Progress.find({}).skip(8000).cursor();
        for (let doc = yield cursor.next(); doc != null; doc = yield cursor.next()) {
            console.log(`${count}: ${doc._id}: ${doc.url}`);
            const good = yield testUrl(doc.url);
            if(!good) {
                console.log(`^ removing: ${doc._id}: ${doc.url}`);
                yield doc.remove();
                removeCount++;
            }
            count++;
        }
        console.log(`count: ${count}, removeCount: ${removeCount}`);
        process.exit();
    });
}());
