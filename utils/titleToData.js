exports.getWeight = (title) => {
    try {
        // Find part of the title that has weight loss numbers, [x > y = z]
        const startIndex = title.indexOf('[') + 1;
        const endIndex = title.indexOf(']');
        const weightStr = title.substr(startIndex, endIndex - startIndex);
        const weightStrArr = weightStr.split('>');
        
        const startWeightStr = weightStrArr[0];
        const endWeightStr = weightStrArr[1];
        const startWeight = parseFloat(startWeightStr);
        const endWeight = parseFloat(endWeightStr);

        if(isNaN(startWeight))
            throw new Error(`Cannot read startWeight in ${title}`);
        else if(isNaN(endWeight))
            throw new Error(`Cannot read endWeight in ${title}`);

        return { startWeight, endWeight };
    }
    catch (e) {
        console.error(`Error reading weight from ${title}`);
        throw new Error(e);
    }
}

exports.getPersonInfo = (title) => {
    try {
        const dataArr = title.split('"')[0].split('/');
        const gender = genderMap[dataArr[0]];
        const age = parseInt(dataArr[1]);
        const height = getHeight(dataArr[2]);
        if(gender !== 'MALE' && gender !== 'FEMALE')
            throw new Error(`Cannot read gender in ${title}`);
        else if(isNaN(age))
            throw new Error(`Cannot read age in ${title}`);
        else if(isNaN(height))
            throw new Error(`Cannot read height in ${title}`);
        return { gender, age, height }
    }
    catch (e) {
        console.error(`Error reading person info from ${title}`);
        throw new Error(e);
    }
}

const getHeight = (str) => {
    try {
        let s = str.split('\'');
        if(s.length === 1)
            s = str.split('’');
        const feet = parseInt(s[0]);
        const inches = parseInt(s[1])
        const totalInches = feet * 12 + inches;
        return totalInches;
    }
    catch (e) {
        console.error(`Error reading height from ${str}`);
        throw new Error(e);
    }
}

const genderMap = {
    'm': 'MALE',
    'M': 'MALE',
    'f': 'FEMALE',
    'F': 'FEMALE',
}